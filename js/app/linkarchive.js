var linkArchive = angular.module('LinkArchive',['ngRoute']);
var linkController = linkArchive.controller('LinkController', function($scope){
	$scope.tags = [{
		name:'programming'},{
		name:'social'},{
		name:'career'},{
		name:'recruiting'},{
		name:'management'},{
		name:'personal'}];
	$scope.links = [{
		name:'My Blog',
		url:'http://www.hascode.com/',
		tags:['programming','personal']
	}];
});

linkArchive.config(function ($routeProvider) {
	$routeProvider.when('/', {
		templateUrl: "js/template/linkarchive/view.html",
		controller: "LinkController"
	})
		.otherwise({
			redirectTo: "/"
		})
});


